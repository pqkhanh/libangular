import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Components
import { HeaderComponent } from './Components/Header/header.component';
import { FooterComponent } from './Components/Footer/footer.component';
import { SideBarComponent } from './Components/Sidebar/sidebar.component';

// Screens
import { HomeScreen } from './Screens/Home/home.component';
import { LoginScreen } from './Screens/Login/login.component';

// Views
import { LoginView } from './Views/Login/login.component';

// Directives
import { Dropdown } from './Directives/Dropdown/dropdown';


@NgModule({
  declarations: [
    AppComponent,
    // Components
    HeaderComponent,
    FooterComponent,
    SideBarComponent,
    // Screens
    HomeScreen,
    LoginScreen,
    // Views
    LoginView,
    // Directives
    Dropdown
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
