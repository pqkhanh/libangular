import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeScreen } from './Screens/Home/home.component'
import { LoginScreen } from './Screens/Login/login.component'


const routes: Routes = [
  {
    path: "home",
    component: HomeScreen
  },
  {
    path: "layout",
    component: HomeScreen,
    children: [
      { path: "login", component: LoginScreen }
    ]
  },
  { path: "login",
    component: LoginScreen 
  },
  { path: "", 
    pathMatch: "full", 
    redirectTo: "/login"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
