import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Components
import { HeaderComponent } from './Components/Header/header.component';
import { FooterComponent } from './Components/Footer/footer.component';
import { SideBarComponent } from './Components/Sidebar/sidebar.component';

// Screens
import { HomeScreen } from './Screens/Home/home.component';
import { LoginScreen } from './Screens/Login/login.component';

// Views
import { LoginView } from './Views/Login/login.component';

// Directives
import { Dropdown } from './Directives/Dropdown/dropdown';


@NgModule({
  declarations: [
    // Components
    HeaderComponent,
    FooterComponent,
    SideBarComponent,
    // Screens
    HomeScreen,
    LoginScreen,
    // Views
    LoginView,
    // Directives
    Dropdown
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule
  ],
  exports: [
    // Components
    HeaderComponent,
    FooterComponent,
    SideBarComponent,
    // Screens
    HomeScreen,
    LoginScreen,
    // Views
    LoginView,
    // Directives
    Dropdown
  ]
})
export class LibKhanhModule { }
