# LibKhanh

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.0.

## How to install

Run 'npm i lib-khanh'
Because this lib is built on bootstrap, So open file `angular.json` and add this code `node_modules/bootstrap/dist/css/bootstrap.min.css`
```javascript
   "architect": {
     "build": {
       ...,
       "styles": [
          "src/styles.scss",
          "node_modules/bootstrap/dist/css/bootstrap.min.css"
        ],
     }
   }
```
# How to use

Import library to project.
```javascript
    import { LibKhanhModule } from 'lib-khanh';
    @NgModule({
      declarations: [
        AppComponent
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        LibKhanhModule
      ],
      providers: [],
      bootstrap: [AppComponent]
    })
    export class AppModule { }
```

Component and Usage

| Component  | Input | Type | Example |
| ------------- | ------------- |------------- | ------------- |
| app-header  | headerData | Object | {"logo":{"link":"/home","image":"https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg"},"menu":[{"link":"/home","text":"Home","isActive":["/layout/login"]},{"link":"#","text":"Features"},{"link":"#","text":"Pricing"}],"profile":{"avatar":"https://mdbootstrap.com/img/Photos/Avatars/avatar-2.jpg","actions":[{"link":"#","text":"Action"},{"link":"#","text":"Action 1"},{"link":"#","text":"Action 3"}]}} |
| app-sidebar | menuData | Array | [{"link":"/login","text":"Login","icon":"fas fa-sign-in-alt"}] |
| app-footer |  |  |  |

Screen and Usage

| Screen  | 
| ------------- | 
| app-home-screen  | 
| app-login-screen |

View and Usage

| View  | Input | Type | Example |
| ------------- | ------------- | ------------- | ------------- | 
| app-login-view  | data | Object | {"email":"khanh@tma.com.vn","password":"12345678"} |
|  | onSubmit | Output | |

Example 

```javascript
    import { HomeScreen } from 'lib-khanh';
    import { LoginScreen } from 'lib-khanh';
    import { LoginView } from 'lib-khanh';

    const routes: Routes = [
      {
        path: "home",
        component: HomeScreen
      },
      {
        path: "login",
        component: LoginScreen
      },
      {
        path: "view/login",
        component: LoginView
      },
      {
        path: "layout",
        component: HomeScreen,
        children: [
          { path: "login", component: LoginScreen }
        ]
      },
    ];

    @NgModule({
      imports: [RouterModule.forRoot(routes)],
      exports: [RouterModule]
    })
```
