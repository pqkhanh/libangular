import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Router} from '@angular/router'

const temp = {
  logo: {
    link: '/home',
    image: 'https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg'
  },
  menu: [
    {
      link: '/home',
      text: 'Home',
      isActive: ['/layout/login']
    },
    {
      link: '#',
      text: 'Features',
    },
    {
      link: '#',
      text: 'Pricing',
    }
  ],
  profile: {
    avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-2.jpg',
    actions: [
      {
        link: '#',
        text: 'Action'
      },
      {
        link: '#',
        text: 'Action 1'
      },
      {
        link: '#',
        text: 'Action 3'
      }
    ] 
  }
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() headerData: any;
  
  constructor(private router: Router) { }

  ngOnInit() {
    if(!this.headerData){
      this.headerData = temp;
    }
  }

  ngAfterContentChecked(){
    
  }

  isActive(menu){
    return this.router.url.indexOf(menu.link) !== -1 || (menu.isActive && menu.isActive.indexOf(this.router.url) !==-1) ;
  }
}
