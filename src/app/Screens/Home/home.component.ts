import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-screen',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeScreen implements OnInit {
  menuData: any = [
    {
      link: '/home',
      text: 'Home',
      icon: 'fas fa-home'
    },
    {
      link: '/login',
      text: 'Login',
      icon: 'fas fa-sign-in-alt'
    },
    {
      link: '/layout/login',
      text: 'Home Login',
      icon: 'fas fa-sign-in-alt'
    }
  ]

  headerData: any = {
    logo: {
      link: '/home',
      image: '../../../assets/images/bootstrap-solid.svg'
    },
    menu: [
      {
        link: '/home',
        text: 'Home',
        isActive: ['/layout/login']
      },
      {
        link: '#',
        text: 'Features',
      },
      {
        link: '#',
        text: 'Pricing 1',
      }
    ],
    profile: {
      avatar: 'https://mdbootstrap.com/img/Photos/Avatars/avatar-2.jpg',
      actions: [
        {
          link: '#',
          text: 'Action '
        },
        {
          link: '#',
          text: 'Action 1'
        },
        {
          link: '#',
          text: 'Action 3'
        }
      ] 
    }
  }
  constructor() { }

  ngOnInit() {
  }

}
