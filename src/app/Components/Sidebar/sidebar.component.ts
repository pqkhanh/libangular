import { Component, OnInit, Input } from '@angular/core';

const temp = [
  {
    link: '/login',
    text: 'Login',
    icon: 'fas fa-sign-in-alt'
  }
]

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SideBarComponent implements OnInit {
  @Input() menuData: any;
  constructor() { }

  ngOnInit() {
    if(!this.menuData){
      this.menuData = temp;
    }
  }

  ngAfterContentChecked(){
  }
}
