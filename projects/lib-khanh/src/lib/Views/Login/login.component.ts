import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-view',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginView implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  @Input() data: any;
  @Output() onSubmit = new EventEmitter();

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    if(!this.data){
      this.data = {
        email: '',
        password: ''
      }
    }
    this.loginForm = this.formBuilder.group({
      email: [this.data.email, [Validators.required, Validators.email]],
      password: [this.data.password, [Validators.required, Validators.minLength(6)]]
    });
  }

  get fields() { return this.loginForm.controls; }

  submit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    if(this.onSubmit){
      this.onSubmit.emit(this.loginForm.value);
    }
  }
}
